import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



@ManagedBean(name="UsersBean")
@SessionScoped
public class Users {
	
	ArrayList<Users> myArrayListOfUsers = new ArrayList<Users>(); 


	// default constructor
	public Users() {
	
	}
	
	// overloaded constructor
	public Users(String firstName, String surname, String emailAddress, String password) {
		this.firstName = firstName;
		this.surname = surname;
		this.emailAddress = emailAddress;
		this.password = password;
	}
	
	// this is returning the hard coded values
//	public String firstName = "Peter";
//	public String surname = "Doyle";
//	public String emailAddress = "peter@gmail.com";
//	public String password = "123";
	
	public String firstName;
	public String surname;
	public String emailAddress;
	public String password;
	
	// to do: should probably put this into a seperate class. Will do it later on. Does the ArrayList reset everytime its called?
	//RegisterBean myBean = new RegisterBean();
	
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
		System.out.println("the first name is: " + getFirstName());
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
		System.out.println("the surname is: " + getSurname());
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		// to do: put some logic in here to make sure the email has an @ - that it is a valid email address
		// to do: put some logic in here to make sure the email has not already been used to make an account
		// to do: put some logic in here to make a difference if its @staff.com
		this.emailAddress = emailAddress;
		System.out.println("the email is: " + getEmailAddress());
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		// to do: put some logic in here to make sure the email address is secure, one letter, one number etc. 
		this.password = password;
		System.out.println("the password is: " + getPassword());
	}
	
	@Override
	public String toString() {
		return "The firstname is: " + getFirstName() + 
				". The surname is: " + getSurname() + 
				". The email is: " + getEmailAddress() +
				". The password is: " + getPassword() ;
	}
	
	public void saveNewUser() {
		System.out.println("Inside saveNewUser");
		Users myNewUsers = new Users(firstName, surname, emailAddress, password);
		
		myArrayListOfUsers.add(myNewUsers);
		
		//to do: can delete this for loop. was only checking that the ArrayList was working correctly.
		for (int i = 0; i < myArrayListOfUsers.size(); i++) {
			System.out.println("information about the new saved person: " + myArrayListOfUsers.get(i));
			System.out.println("the password is: " + myArrayListOfUsers.get(i).getPassword());
			
		}
		
	}
	
	
	

}
